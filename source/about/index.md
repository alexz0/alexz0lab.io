# About me

Hi, I am glad that you find some time to explore this page. My name is Aleksei Zolotykh. I am a software engineer, team leader, speaker and co-organizer of HolyJS conference.

Here you can find the list of my articles on third-party resources and also the list of videos with my public talks.

# Articles

- [Почему я не верю микробенчмаркам](https://habr.com/ru/company/wrike/blog/433060/)
- [Презумпция ума](https://habr.com/ru/company/wrike/blog/427505/)
- [FZF. Нечеткий поиск или как быстро ставить npm пакеты и убивать процессы](https://habr.com/ru/company/wrike/blog/344770/)
- [Оптимизация фронтенда. Часть 2. Чиним tree-shaking в проекте на webpack](https://habr.com/ru/company/wrike/blog/342964/)
- [Оптимизация фронтенда. Часть 1. Почему я не люблю слово treeshaking или где вас обманывает webpack](https://habr.com/ru/company/wrike/blog/342686/)
- [Как отрефакторить 2 500 000 строк кода и не сойти с ума](https://habr.com/ru/company/wrike/blog/334590/)
- [Когда вредно тестировать ваши компоненты](https://habr.com/ru/company/wrike/blog/315908/)
- [Zone.js или как Dart спас Angular](https://habr.com/ru/company/wrike/blog/310422/)
- [Асинхронность в JavaScript: Пособие для тех, кто хочет разобраться](https://habr.com/ru/company/wrike/blog/302896/)
- [Лайфхаки для веб-разработчика](https://habr.com/ru/post/267751/)

# Videos

- [HolyJS](https://holyjs.ru/)
  - [2018 Interview](https://www.youtube.com/watch?v=ossXRlXmZoY)
  - [Архитектурные этюды как не угробить архитектуру своего проекта](https://www.youtube.com/watch?v=rEU7hogGwzA)
  - [Решения, которые мы принимаем. Личный взгляд на архитектуру](https://www.youtube.com/watch?v=xXrXlAvwB8E)
  - [ES2017 vs Typescript vs Dart Сравниваем без эмоций](https://www.youtube.com/watch?v=DLGukTyg4xo&t=2350s)
- [DartUp](https://dartup.ru/)
  - [История самурая. Путь, который проходит Dart-код от IDE до браузера](https://www.youtube.com/watch?v=G6a5YcqNx7g)
  - [About zones in Dart](https://www.youtube.com/watch?v=QIEhkvYRVNg)
  - [Алиса в стране JS. Как DART живёт в браузере](https://youtu.be/Bk-uJgo5GO4)
- [PiterJS](https://piterjs.org/)
  - [PiterJS #14 Лайфхаки фронтэнд разработчика](https://www.youtube.com/watch?v=thVZI3mUXUM)
  - [PiterJS #22 Рефакторинг](https://www.youtube.com/watch?v=hfmHymOCIA0)
  - [Автоматизируй это. Гиперавтоматизированный пайплайн](https://www.youtube.com/watch?v=F7JZlLIq4ZY)
- [Secon](https://secon.ru/)
  - [GraphQL — Новый Redux](https://www.youtube.com/watch?v=zJdNgCk4K9M)
  - [Разговор с Алексеем Золотых о тщеславии фронтенд-разработчика](https://youtu.be/PB5DYqy7IPM)
- [Frontend conf](https://frontendconf.ru/)
  - [Рефакторинг клиентского кода](https://www.youtube.com/watch?v=wWOZ7PLFWi8)
  - [Лайфхаки для фронтенд-разработчиков](https://www.youtube.com/watch?v=IHPjaECmtCo)
- [Codefest](https://codefest.ru)
  - [Сжимаем Javascript по-взрослому](https://www.youtube.com/watch?v=feptFoey_bU)
- [SECR](https://secrus.org)
  - [Один язык – все платформы](https://www.youtube.com/watch?v=bYjsdmrrFTI)
- [TeamLead conf](https://teamleadconf.ru/)
  - [Китайские стратагемы Сунь-цзы и как они помогают мне в работе](https://www.youtube.com/watch?v=VIhiFKMfYXA)

# Podcasts

- [Разговор с Алексеем Золотых про Dart](https://www.youtube.com/watch?v=CP6NO-XEuls)
- [Frontend Weekend #103](https://soundcloud.com/frontend-weekend/fw-103)
- [HolyJS Online: Undefined Podcast](https://www.youtube.com/watch?v=cJPKabtSH0I&feature=emb_title)
- [#127 Мало половин. Часть вторая](https://vk.com/podcast-151803527_456239245)
- [#146 Web Standards](https://youtu.be/RB6TjhfLvxE?list=PLMBnwIwFEFHcwuevhsNXkFTcadeX5R1Go)

# HolyJS. Тяжелое утро

- [Тяжелое утро с HolyJS #1](https://www.youtube.com/watch?v=xJw7iQKp2cI)
- [Тяжелое утро с HolyJS #2](https://www.youtube.com/watch?v=ar7-HXBYfnk)
- [Маргарита Рой / Тяжелое утро с HolyJS](https://www.youtube.com/watch?v=CeoQ8cDWy_Y)
- [Тяжелое утро с HolyJS / Фил Ранжин, Евгений Кот, Алексей Золотых ](https://www.youtube.com/watch?v=KhB_V2lHhJE)
- [Тяжелое утро с HolyJS / ПК HolyJS ](https://youtu.be/xaZ2bu0ld20)
- [Тяжелое утро с HolyJS / Павел Малышев, Евгений Кот, Алексей Золотых](https://youtu.be/4mdNDavIGpc)
- [Тяжелое утро с HolyJS / Наталья Теплухина, Евгений Кот, Алексей Золотых](https://youtu.be/J5Xqhp3xLXM)
- [Тяжелое утро с HolyJS / Андрей Ситник, Евгений Кот, Алексей Золотых](https://youtu.be/sfq_t89A9_o)
- [Тяжелое утро с HolyJS / Обзор программы HolyJS](https://youtu.be/BG2_M5VtqqM)
- [Тяжелое утро с HolyJS / Энжи Сказка](https://www.youtube.com/watch?v=Zx0tMVo0zgk)
